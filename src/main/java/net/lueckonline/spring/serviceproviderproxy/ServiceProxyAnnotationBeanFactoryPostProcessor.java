package net.lueckonline.spring.serviceproviderproxy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AutowireCandidateResolver;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

/**
 * additional BeanFactoryPostProcessor that is only used to override the {@link AutowireCandidateResolver} on the beanfactory.
 * 
 * @author thuri
 *
 */
public class ServiceProxyAnnotationBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    
    if(beanFactory instanceof DefaultListableBeanFactory) {
      DefaultListableBeanFactory defaultBf = (DefaultListableBeanFactory) beanFactory;
      defaultBf.setAutowireCandidateResolver(new ServiceProxyAnnotationAutowireCandidateResolver());
    }
  }

}
