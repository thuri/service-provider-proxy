package net.lueckonline.spring.serviceproviderproxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annotation to mark a bean to be used as target of a method call on an
 * interface annotated with {@link ServiceProviderProxy} when the 
 * {@link #expression()} is evaluated to true at runtime
 * 
 * @author thuri
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ProxiedService {
  String expression();
}