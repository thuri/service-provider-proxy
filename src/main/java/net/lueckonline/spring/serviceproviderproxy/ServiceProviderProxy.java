package net.lueckonline.spring.serviceproviderproxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annotation to mark an interface to be proxied 
 * and that the actual target of calls to this interface are to be 
 * selected at runtime 
 * 
 * @author thuri
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceProviderProxy {
  
}