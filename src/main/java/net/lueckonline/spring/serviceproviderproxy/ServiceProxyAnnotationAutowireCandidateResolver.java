package net.lueckonline.spring.serviceproviderproxy;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

/**
 * class inspired by @see{ContextAnnotationAutowireCandidateResolver} especially the creation of the proxy object
 * 
 * creates a proxy for all {@link Autowire}d references to interfaces annotated with {@link ServiceProviderProxy}.
 * when the proxy is called it looks for all beans implementing the interface and whose classes are annotated with {@link ProxiedService}
 * and then evaluates the expresson of that annotation to select exactly one bean to which the call is then deferred. 
 * 
 * @author thuri
 *
 */
public class ServiceProxyAnnotationAutowireCandidateResolver extends ContextAnnotationAutowireCandidateResolver {

  @Override
  public Object getLazyResolutionProxyIfNecessary(DependencyDescriptor descriptor, String beanName) {
    
    if(Stream.of(descriptor.getField().getType().getAnnotations())
      .map(ann -> AnnotationUtils.getAnnotation(ann, ServiceProviderProxy.class))
      .filter(ann -> ann != null)
      .findFirst().isPresent()) {
      
      Assert.state(getBeanFactory() instanceof DefaultListableBeanFactory,"BeanFactory needs to be a DefaultListableBeanFactory");
      final DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) getBeanFactory();
      
      TargetSource ts = new TargetSource() {
        
        @Override
        public Class<?> getTargetClass() {
          return descriptor.getDependencyType();
        }
        @Override
        public boolean isStatic() {
          return false;
        }
        
        @Override
        public void releaseTarget(Object target) {
        }
        
        @Override
        public Object getTarget() throws Exception {
          
          Map<String, ?> beansOfType = beanFactory.getBeansOfType(descriptor.getField().getType());
          
          BeanExpressionContext expressionContext = (beanFactory != null ? new BeanExpressionContext(beanFactory, null) : null);
          
          Map<String, ?> matchingBeans = beansOfType.entrySet().stream()
          .filter(e -> {
            ProxiedService ann = e.getValue().getClass().getAnnotation(ProxiedService.class);
            if(ann != null)
            {
              String value = beanFactory.resolveEmbeddedValue(ann.expression());
              Object evaluate = beanFactory.getBeanExpressionResolver().evaluate(value, expressionContext);
              if(evaluate != null && evaluate instanceof Boolean)
                return (Boolean) evaluate;
              else
                return false;
            }
            else return false;
          }).collect(Collectors.toMap(e -> e.getKey(), e-> e.getValue()));
          
          if(matchingBeans.size() >  1) throw new IllegalStateException("Found "+matchingBeans.size()+" beans with a matching proxy condition. Can't decide which to use:"+matchingBeans.keySet().toString());
          if(matchingBeans.size() == 0) throw new IllegalStateException("Couldn't find any matching Bean for type "+descriptor.getField().getType().getName());
          
          return matchingBeans.values().iterator().next();
        }
      };
      
      ProxyFactory pf = new ProxyFactory();
      pf.setTargetSource(ts);
      Class<?> dependencyType = descriptor.getDependencyType();
      if (dependencyType.isInterface()) {
        pf.addInterface(dependencyType);
      }
      return pf.getProxy(beanFactory.getBeanClassLoader());
      
    }
    else 
      return super.getLazyResolutionProxyIfNecessary(descriptor, beanName);
      
  }
}
