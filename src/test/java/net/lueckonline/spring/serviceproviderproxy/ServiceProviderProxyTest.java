package net.lueckonline.spring.serviceproviderproxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ServiceProviderProxyTest {
  
  private AnnotationConfigApplicationContext ac;
  private ProxiedServiceClient client;
  private CaseSwitch caseSwitch; 
  
  @Before
  public void setup() {
    
    // setup the application context ...
    
    ac = new AnnotationConfigApplicationContext(); 
    // .. add the bfpp that sets the special AutowireCandidateResolver
    ac.addBeanFactoryPostProcessor(new ServiceProxyAnnotationBeanFactoryPostProcessor());
    
    // .. register a bean definition for the client code that calls the "interface"
    RootBeanDefinition abd = new RootBeanDefinition(ProxiedServiceClient.class);
    abd.setScope(RootBeanDefinition.SCOPE_PROTOTYPE);
    ac.registerBeanDefinition("annotatedBean", abd);
    
    // ..first bean implementing the proxied interface. This should be called when
    // the switchValue in the "switch" bean has the value "B2B" 
    RootBeanDefinition p1bd = new RootBeanDefinition(B2BServiceImpl.class);
    ac.registerBeanDefinition("serviceImpl1", p1bd);
    
    // .. same as before only this time the expression of the implementing class is different
    RootBeanDefinition p2bd = new RootBeanDefinition(B2CServiceImpl.class);
    ac.registerBeanDefinition("serviceImpl2", p2bd);
    
    // .. finally add the switch bean which is only needed to have something to be evaluated :-)
    RootBeanDefinition sbd = new RootBeanDefinition(CaseSwitch.class);
    ac.registerBeanDefinition("switch", sbd);
    ac.refresh();
    
    assertTrue(ac.getBeanFactory().containsSingleton("serviceImpl1"));
    assertTrue(ac.getBeanFactory().containsSingleton("serviceImpl2"));
    assertTrue(ac.getBeanFactory().containsSingleton("switch"));
   
    // ... get the client bean so that we can call it in the test
    client = ac.getBean("annotatedBean", ProxiedServiceClient.class);
    // .. get the switch bean so that we can change the value in the test
    caseSwitch = ac.getBean("switch", CaseSwitch.class);
  }
  
  @After
  public void teardown() {
    ac.close();
  }
  
  @Test
  public void testProxiedResourceInjectionWithField() {
   
    // ... set the switch so that the expression on the first bean is true
    caseSwitch.switchValue = "B2B";
    assertEquals("Well you know ... ", client.proxiedService.doCoolStuff());
    
    // ... no change the value so that the other expression will return true
    caseSwitch.switchValue = "B2C";
    assertEquals("This will do ...", client.proxiedService.doCoolStuff());
  }
  
  public static class ProxiedServiceClient {
    @Autowired 
    public ProxiedServiceInterface proxiedService;
  }
  
  @ServiceProviderProxy
  public static interface ProxiedServiceInterface {
    public String doCoolStuff();
  }
  
  @ProxiedService(expression = "#{switch.switchValue == 'B2B'}")
  public static class B2BServiceImpl implements ProxiedServiceInterface {
    @Override
    public String doCoolStuff() {
      return "Well you know ... ";
    }
  }
  
  @ProxiedService(expression = "#{switch.switchValue == 'B2C'}")
  public static class B2CServiceImpl implements ProxiedServiceInterface {
    @Override
    public String doCoolStuff() {
      return "This will do ...";
    }
  }
  
  public static class CaseSwitch {
    public String switchValue = "";
  }

}
